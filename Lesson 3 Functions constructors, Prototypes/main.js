const Big_Size = {
    name:'Big_Size',
    price: 30,
    calories: 30
};
const Small_Size = {
    name:'Small_Size',
    price: 10,
    calories: 10
};
const Topping_Mayo = {
    name:'Topping_Mayo',
    price: 5,
    calories: 3
};

function HotDog(options) {
   options = options || {};
    this.size = options.size;
    this.topping = options.topping || [];

    if (options.size === !Big_Size || options.size === !Small_Size) {
        throw console.log('Error! Invalid size');
    }
    if (this.topping !== undefined && !this.topping.includes(Topping_Mayo)) {
        throw console.log('Error! This topping is not find');
    }
}

HotDog.prototype.addTopping = function(topping) {
    this.topping.push(topping);
}

HotDog.prototype.countPrice = function() {
    let toppingPrice = this.topping[0].price || 0;

    return this.size.price + toppingPrice;
}

HotDog.prototype.countCalories = function() {
    let toppingCalories = this.topping[0].calories || 0;

    return this.size.calories + toppingCalories;
}


hotDog = new HotDog({
    size:Big_Size,
    topping:[Topping_Mayo]
});

hotDog2 = new HotDog({
    size:Small_Size,
    topping:[Topping_Mayo]
});

console.log(hotDog);
console.log('Price', hotDog.countPrice());
console.log('Calories', hotDog.countCalories());


console.log(hotDog2);
console.log('Price', hotDog2.countPrice());
console.log('Calories', hotDog2.countCalories());



let phone = "+380975305852";
console.log(/^\+[0-9]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone));