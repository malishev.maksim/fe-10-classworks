let arrowFunc = (a, b) => a + b;
    let arrowFunc2 = (a, b) => {
      a + b;
    };

    let arrowFunc3 = (a, b) => {
      return a + b;
    };

    console.log(arrowFunc(3, 4));
    console.log(arrowFunc2(3, 4));
    console.log(arrowFunc3(3, 4));

    let o = {
      name: "Max",
      myMethod: () => this.name
    }

    console.log(o.myMethod());
    console.log(o.myMethod.apply(o));//не прибивается this через эти методы (apply и call и bind)

    // привязка this в Timeout через доп переменную, в которую записываем функцию и bind
    let showName = function () {
      alert("Your name is " + this.name);
    };
    let myTimeout = setTimeout(showName.bind(o), 1000);
    //То же самое, но без создания дополнительной переменной
    setTimeout(function () {
      alert("2 Your name is " + this.name);      
    }.bind(o), 1500);