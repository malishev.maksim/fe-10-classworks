function makeAdder(x) {
  console.log(`x: ${x}`);
  return function(y) {
  console.log(`y: ${y}`);
    return x + y;
  };
}

var add5 = makeAdder(5); //Именно в этот момент я дополняю лексическое окружение иксом = 5

console.log(add5);

console.log(add5(2));  //  x=5