let a = {
  name: "Max"
};
let b = {
  name: "John"
};

function showName(z,y) {
  return this.name + z + y;
}

a.showMyName = showName;
b.showMyName = showName;

console.log(a.showMyName("hello", "hello2"));

//apply и call различаются только синтаксисом передачи параметров
//они сразу вызывают функцию
console.log(a.showMyName.apply(b, ["hello", "hello2"]));
console.log(a.showMyName.call(b, "hello", "hello2"));

//bind не вызывает функцию, и возвращает другую функцию, не изменяя исходную
console.log(a.showMyName.bind(b, "hello", "hello2"));
const c = a.showMyName.bind(b, "hello", "hello2");
console.log(c());

//В setTimiout нужно использовать bind, т.к. call и apply вызывают фукнцию.